'use strict'
const { resolve } = require('path')
require('ts-node')
  .register({ transpileOnly: true, project: resolve(__dirname, 'tsconfig.json') })
