# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2021-04-25

### Added

- Next releases will be published as commonjs and ES modules.
- An additional string property named `full` is added to the output object.
  It contains the full catalog data as one XML document.
  The properties representing one document per endpoint remain untouched.
- More types for usage with typescript

### Changed

- The code steadily became more complex, so I decided to migrate it to typescript.
  This shouldn't affect usage,
  because the package is transpiled to commonjs which it was before.
  I already used JSDoc types, so migration was not very difficult.

## [0.7.1] - 2021-04-16

### Fixed

- Updated deps

## [0.7.0] - 2021-03-10

### Changed

- The changes of v0.6.0 must be reverted,
  because RDMO is not able to safely import elements in a suitable order,
  and import fails when the file to be imported is to large.
- So write function returns an object with an XML document for each endpoint.

## [0.6.0] - 2021-03-09

### Changed

- The complete catalog is written into one single XML document,
  where the nodes are sorted by endpoint (domain, options, conditions, questions).
- The conditions import issue seems to be fixed,
  so there's no more need for partitionning into multiple documents.

## [0.5.0] - 2021-03-08

### Changed

- Condition key generation has changed because the keys were too long:
  - Create SHA256 hash of the stringified condition object
  - Return the first 10 characters

## [0.4.1] - 2021-02-08

### Added

- Serializes unit property in questions with numeric response

## [0.4.0] - 2021-02-07

### Added

- Utility function `readFromDir` which reads catalog data from XML files in directory
- Integration test

### Changed

- Every endpoint is serialized into its own XML document
  in order to prevent import issues
- Updated to newest schema and implemented additional question types

### Fixed

- Serialization of conditions with option target:
  Target was simply passed as text instead of creating an option reference

## [0.3.2] - 2021-01-27

### Fixed

- Always add order element with value 0 to serialized optionset elements,
  because importing optionsets without order into RDMO fails

## [0.3.1] - 2021-01-27

### Fixed

- Always add order element with value 0 to serialized catalog elements,
  because importing catalogs without order into RDMO fails

## [0.3.0] - 2021-01-26

### Changed

- Updated to newest schema which allows a version property
- Disambiguates elements by serializing keys with group and version
- The catalog key will be prepended to the key
  of each parentless non-catalog element.
- If a JSON document has a version, it will be appended to the key
  of each parentless element.

## [0.2.2] - 2021-01-21

### Fixed

- Overwritten attributes in state map

## [0.2.1] - 2021-01-20

### Changed

- Update to newest schema

## [0.2.0] - 2021-01-18

### Changed

- Updated to schema v0.5.0
- Moved extraction logic into one write function,
  instead of having one function for each element type

### Added

- Serialize conditions and autogenerate condition keys
- Serialize questions with attribute, conditions,
  and properties depending on question type

## [0.1.3] - 2021-01-13

### Fixed

- Failing imports into RDMO due to non-capitalized booleans:
  Booleans are now serialized as `True` and `False`.

## [0.1.2] - 2021-01-11

### Added

- Typescript declaration files

## [0.1.1] - 2021-01-10

### Changed

- Project has been moved to gitlab

## [0.1.0] - 2020-12-22

### Added

- Serialization functions for domain, options, and question catalog

[Unreleased]: https://gitlab.com/rdmo-json/xml/compare/v0.8.0...main
[0.8.0]: https://gitlab.com/rdmo-json/xml/compare/v0.7.1...v0.8.0
[0.7.1]: https://gitlab.com/rdmo-json/xml/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/rdmo-json/xml/compare/v0.6.0...v0.7.0
[0.6.0]: https://gitlab.com/rdmo-json/xml/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/rdmo-json/xml/compare/v0.4.1...v0.5.0
[0.4.1]: https://gitlab.com/rdmo-json/xml/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/rdmo-json/xml/compare/v0.3.2...v0.4.0
[0.3.2]: https://gitlab.com/rdmo-json/xml/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/rdmo-json/xml/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/rdmo-json/xml/compare/v0.2.2...v0.3.0
[0.2.2]: https://gitlab.com/rdmo-json/xml/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/rdmo-json/xml/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/rdmo-json/xml/compare/v0.1.3...v0.2.0
[0.1.3]: https://gitlab.com/rdmo-json/xml/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/rdmo-json/xml/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/rdmo-json/xml/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/rdmo-json/xml/tags/v0.1.0

<!-- markdownlint-configure-file { "MD024": { "siblings_only": true }} -->
