import * as path from 'path'
import { readFileSync } from 'fs'
import test from 'ava'
import { write } from '../src'
import catalog from './catalog.json'

const entries = ['domain', 'options', 'conditions', 'questions', 'full']
  .map(k => [k, readFileSync(path.resolve('./integration/fixtures', `${k}.xml`)).toString()])
const xml = Object.fromEntries(entries)

test('integration', t => {
  const xml2 = write(catalog)
  t.deepEqual(xml, xml2)
})
