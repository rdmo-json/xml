import test from 'ava'
import { Endpoint } from './reference'
import { ReferenceManager } from './reference-manager'
const prefix = 'https://rdmo.example.com/terms'

test('no config', t => {
  const uris = new ReferenceManager(Endpoint.domain, prefix)
  const usage = uris.use(['entity', 'attribute'])
  t.is(usage.uri, 'https://rdmo.example.com/terms/domain/entity/attribute')
  t.false(uris.isValid())
  const definition = uris.define(['entity', 'attribute'])
  t.is(definition.uri, 'https://rdmo.example.com/terms/domain/entity/attribute')
})

test('More usages than definitions should be invalid', t => {
  const uris = new ReferenceManager(Endpoint.domain, prefix)
  t.false(uris.isUsed(['entity', 'attribute']))
  uris.use(['entity', 'attribute'])
  t.true(uris.isUsed(['entity', 'attribute']))
  uris.use(['entity2', 'attribute2'])
  t.false(uris.isDefined(['entity', 'attribute']))
  uris.define(['entity', 'attribute'])
  t.true(uris.isDefined(['entity', 'attribute']))
  t.false(uris.isValid())
})

test('More definitions than usages should be invalid', t => {
  const uris = new ReferenceManager(Endpoint.domain, prefix)
  uris.use(['entity', 'attribute'])
  uris.define(['entity2', 'attribute2'])
  uris.define(['entity', 'attribute'])
  t.true(uris.isValid())
})

test('Missing definitions should be invalid', t => {
  const uris = new ReferenceManager(Endpoint.domain, prefix)
  uris.define(['entity2', 'attribute'])
  uris.use(['entity', 'attribute'])
  t.false(uris.isValid())
})

test('No missing definitions should be valid', t => {
  const uris = new ReferenceManager(Endpoint.domain, prefix)
  uris.use(['entity', 'attribute'])
  uris.define(['entity', 'attribute'])
  t.true(uris.isValid())
})

test('group', t => {
  const uris = new ReferenceManager(Endpoint.options, prefix, { group: 'sfb_135' })
  const usage = uris.use(['optionset', 'option'])
  t.is(usage.uri, 'https://rdmo.example.com/terms/options/sfb_135_optionset/option')
})

test('version', t => {
  const uris = new ReferenceManager(Endpoint.options, prefix, { version: '0.1.0' })
  const usage = uris.use(['optionset', 'option'])
  t.is(usage.uri, 'https://rdmo.example.com/terms/options/optionset_0.1.0/option')
})

test('group and version', t => {
  const uris = new ReferenceManager(Endpoint.options, prefix, { group: 'sfb_135', version: '0.1.0' })
  const usage = uris.use(['optionset', 'option'])
  t.is(usage.uri, 'https://rdmo.example.com/terms/options/sfb_135_optionset_0.1.0/option')
})

test('Double usage should cache', t => {
  const uris = new ReferenceManager(Endpoint.options, prefix, { group: 'sfb_135', version: '0.1.0' })
  const x = uris.use(['optionset', 'option'])
  const y = uris.use(['optionset', 'option'])
  t.true(x === y)
})

test('Definition after usage should cache', t => {
  const uris = new ReferenceManager(Endpoint.options, prefix, { group: 'sfb_135', version: '0.1.0' })
  const x = uris.use(['optionset', 'option'])
  const y = uris.define(['optionset', 'option'])
  t.true(x === y)
})

test('Usage after definition should cache', t => {
  const uris = new ReferenceManager(Endpoint.options, prefix, { group: 'sfb_135', version: '0.1.0' })
  const y = uris.define(['optionset', 'option'])
  const x = uris.use(['optionset', 'option'])
  t.true(x === y)
})

test('Double definition should throw', t => {
  const uris = new ReferenceManager(Endpoint.options, prefix, { group: 'sfb_135', version: '0.1.0' })
  uris.define(['optionset', 'option'])
  t.throws(() => uris.define(['optionset', 'option']))
})
