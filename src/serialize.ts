import { create, fragment } from 'xmlbuilder2'
import type { XMLBuilder } from 'xmlbuilder2/lib/interfaces'
import type { Option, ReadableText, Relation } from '@rdmo-author/schema'
import type { Reference } from './reference'

/**
 * Serializes RDMO XML structures from data,
 * @module
*/

export type Node = XMLBuilder
export type Collection = { [K in 'name' | 'pluralName']: ReadableText }
export interface Range { minimum: number, maximum: number, step?: number }

export interface AnswerType {
  widget: string
  value: string
  optionsets?: Reference[]
  collection?: Collection
  range?: Range
  unit?: string
}

/**
 * Create a root document containing a rdmo container node
 * @param contents
*/
export function root (contents?: Set<XMLBuilder>): XMLBuilder {
  const doc = create({ version: '1.0', encoding: 'UTF-8' })
    .ele('rdmo', { 'xmlns:dc': 'http://purl.org/dc/elements/1.1/' })
  if (contents !== undefined) {
    contents.forEach(node => doc.import(node))
  }
  return doc
}

/**
 * A basic RDMO element
 */
interface BaseElement {
  reference: Reference
  comment?: string
  order?: number
}

/**
 * Serializes a basic RDMO element
 * @private
 * @param base
*/
function base ({ reference, comment = '', order }: BaseElement): {[k: string]: any} {
  const element: {[k: string]: any} = {
    ...uri(reference),
    uri_prefix: reference.prefix,
    key: reference.key
  }
  if (reference.pathSegments.length > 1) { element.path = reference.path }
  element['dc:comment'] = comment
  if (typeof order === 'number') { element.order = order }
  return element
}

/**
 * @param baseElement
 * @param parentReference
*/
export function attribute (baseElement: BaseElement, parentReference?: Reference): XMLBuilder {
  const element = {
    ...base(baseElement),
    ...uri(parentReference, 'parent')
  }
  return fragment({ attribute: element })
}

/**
 * @param baseElement
 * @param source
 * @param relation
 * @param target
*/
export function condition (baseElement: BaseElement, source: Reference, relation: Relation, target: Reference|string): XMLBuilder {
  const element = {
    ...base(baseElement),
    ...uri(source, 'source'),
    relation,
    target_text: '',
    target_option: {}
  }
  if (typeof target === 'string') {
    element.target_text = target
  } else {
    element.target_option = uri(target)
  }
  return fragment({ condition: element })
}

/**
 * @param baseElement
 * @param [conditions=[]]
*/
export function optionset (baseElement: BaseElement, conditions: Reference[] = []): XMLBuilder {
  const element = {
    ...base(Object.assign({ order: 0 }, baseElement)),
    provider_key: '',
    conditions: uri(conditions, 'condition')
  }
  return fragment({ optionset: element })
}

/**
 * @param baseElement
 * @param optionset
 * @param option
*/
export function option (baseElement: BaseElement, optionset: Reference, option: Option): XMLBuilder {
  const element = {
    ...base(Object.assign({ order: 0 }, baseElement)),
    ...uri(optionset, 'optionset'),
    text: translate(option.text),
    additional_input: bool(option.additionalInput ?? false)
  }
  return fragment({ option: element })
}

/**
 * @param baseElement
 * @param title
*/
export function catalog (baseElement: BaseElement, title: ReadableText): XMLBuilder {
  const element = {
    ...base(Object.assign({ order: 0 }, baseElement)),
    title: translate(title)
  }
  return fragment({ catalog: element })
}

/**
 * @param baseElement
 * @param catalog
 * @param title
*/
export function section (baseElement: BaseElement, catalog: Reference, title: ReadableText): XMLBuilder {
  const element = {
    ...base(Object.assign({ order: 0 }, baseElement)),
    ...uri(catalog, 'catalog'),
    title: translate(title)
  }
  return fragment({ section: element })
}

/**
 * @param baseElement
 * @param section
 * @param text
 * @param [conditions]
 * @param [collection]
*/
export function questionset (baseElement: BaseElement, section: Reference, text: { title: ReadableText, help?: ReadableText }, conditions: Reference[] = [], collection?: { name: string, pluralName: string, attribute: Reference }): XMLBuilder {
  const element = {
    ...base(Object.assign({ order: 0 }, baseElement)),
    attribute: uri(collection?.attribute),
    ...uri(section, 'section'),
    is_collection: bool(collection != null),
    help: translate(text.help ?? ''),
    title: translate(text.title),
    verbose_name: translate(collection?.name ?? ''),
    verbose_name_plural: translate(collection?.pluralName ?? ''),
    conditions: uri(conditions, 'condition')
  }
  return fragment({ questionset: element })
}

/**
 * @param baseElement
 * @param questionset
 * @param attribute
 * @param text
 * @param answerType
 * @param conditions
*/
export function question (baseElement: BaseElement, questionset: Reference, attribute: Reference, text: { text: ReadableText, help?: ReadableText }, answerType: AnswerType, conditions: Reference[] = []): XMLBuilder {
  const element = {
    ...base(Object.assign({ order: 0 }, baseElement)),
    ...uri(attribute, 'attribute'),
    ...uri(questionset, 'questionset'),
    is_collection: bool(answerType.collection != null),
    help: translate(text.help ?? ''),
    text: translate(text.text),
    verbose_name: translate(answerType.collection?.name ?? ''),
    verbose_name_plural: translate(answerType.collection?.pluralName ?? ''),
    widget_type: answerType.widget,
    value_type: answerType.value,
    ...Object.assign({ minimum: '', maximum: '', step: '' }, answerType.range ?? {}),
    unit: answerType.unit ?? '',
    optionsets: uri(answerType.optionsets ?? [], 'optionset'),
    conditions: uri(conditions, 'condition')
  }
  return fragment({ question: element })
}

/**
 * Create an element with a dc:uri attribute
 * @private
 * @param reference
 * @param name
*/
function uri (reference: Reference|Reference[]|null = null, name: string = ''): object|object[] {
  let element
  if (reference == null) {
    element = {}
  } else if (Array.isArray(reference)) {
    element = reference.map(x => uri(x))
  } else {
    element = { '@dc:uri': reference.uri }
  }
  if (name === '') {
    return element
  } else {
    const namedElement: {[k: string]: object|object[]} = {}
    namedElement[name] = element
    return namedElement
  }
}

/**
 * @private
 * @param contents
*/
function translate (contents: ReadableText): object[] {
  if (typeof contents === 'string') {
    return [
      { '@lang': 'de', '#': contents },
      { '@lang': 'en', '#': contents }
    ]
  }
  return [
    { '@lang': 'de', '#': contents.de },
    { '@lang': 'en', '#': contents.en }
  ]
}

function bool (value: boolean): 'True'|'False' {
  return value ? 'True' : 'False'
}
