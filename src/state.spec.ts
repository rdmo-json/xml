import test from 'ava'
import { ReferenceState } from './state'

test('reference state', t => {
  const uris = new ReferenceState('https://rdmo.example.com', { group: 'xyz', version: '0.1.0' })
  t.true(uris.validate())
})

test('reference state with invalid domain', t => {
  const uris = new ReferenceState('https://rdmo.example.com', { group: 'xyz', version: '0.1.0' })
  uris.domain.use(['attribute'])
  t.throws(() => uris.validate())
  uris.domain.define(['attribute'])
  t.true(uris.validate())
})

test('reference state with invalid options', t => {
  const uris = new ReferenceState('https://rdmo.example.com', { group: 'xyz', version: '0.1.0' })
  uris.options.use(['optionset', 'option'])
  t.throws(() => uris.validate())
  uris.options.define(['optionset', 'option'])
  t.true(uris.validate())
})

test('reference state with invalid conditions', t => {
  const uris = new ReferenceState('https://rdmo.example.com', { group: 'xyz', version: '0.1.0' })
  uris.conditions.use(['condition'])
  t.throws(() => uris.validate())
  uris.conditions.define(['condition'])
  t.true(uris.validate())
})

test('reference state with invalid questions', t => {
  const uris = new ReferenceState('https://rdmo.example.com', { group: 'xyz', version: '0.1.0' })
  uris.questions.use(['catalog', 'section'])
  t.throws(() => uris.validate())
  uris.questions.define(['catalog', 'section'])
  t.true(uris.validate())
})
