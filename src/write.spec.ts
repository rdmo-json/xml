import test from 'ava'
import { write, flattenDomain, answerProperties } from './write'

test('Invalid object', t => {
  t.throws(() => write('xyz'))
})

test('visidDomain', t => {
  const domain = [
    { foo: ['x', 'y', 'z'] },
    { bar: ['x', 'y', 'z'] },
    {
      baz: [
        { x: ['a', 'b', 'c'] }
      ]
    },
    'xyz'
  ]
  const nodes = flattenDomain(domain)
  t.is(nodes.length, 14)
  t.deepEqual(nodes, [
    { key: 'foo', parentPath: [] },
    { key: 'x', parentPath: ['foo'] },
    { key: 'y', parentPath: ['foo'] },
    { key: 'z', parentPath: ['foo'] },
    { key: 'bar', parentPath: [] },
    { key: 'x', parentPath: ['bar'] },
    { key: 'y', parentPath: ['bar'] },
    { key: 'z', parentPath: ['bar'] },
    { key: 'baz', parentPath: [] },
    { key: 'x', parentPath: ['baz'] },
    { key: 'a', parentPath: ['baz', 'x'] },
    { key: 'b', parentPath: ['baz', 'x'] },
    { key: 'c', parentPath: ['baz', 'x'] },
    { key: 'xyz', parentPath: [] }
  ])
})

test('Answer properties of yes/no question', t => {
  t.deepEqual(
    answerProperties({ type: 'yes-no' }),
    { widget: 'yesno', value: 'boolean' }
  )
})

test('Answer properties of single-choice question', t => {
  t.deepEqual(
    answerProperties({ type: 'single-choice' }),
    { widget: 'radio', value: 'option' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-choice', style: 'radio' }),
    { widget: 'radio', value: 'option' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-choice', style: 'dropdown' }),
    { widget: 'select', value: 'option' }
  )
})

test('Answer properties of multiple-choice question', t => {
  t.deepEqual(
    answerProperties({ type: 'multiple-choice' }),
    { collection: { name: '', pluralName: '' }, widget: 'checkbox', value: 'option' }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-choice', style: 'checkbox' }),
    { collection: { name: '', pluralName: '' }, widget: 'checkbox', value: 'option' }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-choice', style: 'dropdown' }),
    { collection: { name: '', pluralName: '' }, widget: 'select', value: 'option' }
  )
})

test('Answer properties of single-response question', t => {
  t.deepEqual(
    answerProperties({ type: 'single-response' }),
    { widget: 'text', value: 'text' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'text' } }),
    { widget: 'text', value: 'text' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'text', multiline: true } }),
    { widget: 'textarea', value: 'text' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'integer' } }),
    { widget: 'text', value: 'integer' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'decimal' } }),
    { widget: 'text', value: 'float' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'integer', unit: 'tb' } }),
    { widget: 'text', value: 'integer', unit: 'tb' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'decimal', unit: 'tb' } }),
    { widget: 'text', value: 'float', unit: 'tb' }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'integer', range: [0, 5, 1] } }),
    { widget: 'range', value: 'integer', range: { minimum: 0, maximum: 5, step: 1 } }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'decimal', range: [0, 5, 1] } }),
    { widget: 'range', value: 'float', range: { minimum: 0, maximum: 5, step: 1 } }
  )
  t.deepEqual(
    answerProperties({ type: 'single-response', response: { format: 'date' } }),
    { widget: 'date', value: 'datetime' }
  )
})

test('Answer properties of multiple-response question', t => {
  t.deepEqual(
    answerProperties({ type: 'multiple-response' }),
    { collection: { name: '', pluralName: '' }, widget: 'text', value: 'text' }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-response', response: { format: 'text' } }),
    { collection: { name: '', pluralName: '' }, widget: 'text', value: 'text' }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-response', response: { format: 'text', multiline: true } }),
    { collection: { name: '', pluralName: '' }, widget: 'textarea', value: 'text' }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-response', response: { format: 'integer' } }),
    { collection: { name: '', pluralName: '' }, widget: 'text', value: 'integer' }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-response', response: { format: 'decimal' } }),
    { collection: { name: '', pluralName: '' }, widget: 'text', value: 'float' }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-response', response: { format: 'integer', range: [0, 5, 1] } }),
    { collection: { name: '', pluralName: '' }, widget: 'range', value: 'integer', range: { minimum: 0, maximum: 5, step: 1 } }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-response', response: { format: 'decimal', range: [0, 5, 1] } }),
    { collection: { name: '', pluralName: '' }, widget: 'range', value: 'float', range: { minimum: 0, maximum: 5, step: 1 } }
  )
  t.deepEqual(
    answerProperties({ type: 'multiple-response', response: { format: 'date' } }),
    { collection: { name: '', pluralName: '' }, widget: 'date', value: 'datetime' }
  )
})
