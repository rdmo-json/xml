import crypto from 'crypto'
import type { Condition } from '@rdmo-author/schema'

/**
 * Generates a sha256 hash from a string and returns the first n characters
 * @param str - A string to be hashed
 * @param n - The number of characters to take from the hash
*/
function hash (str: string, n: number = 10): string {
  const secret = 'This is a public secret 🤫'
  const sha256Hasher = crypto.createHmac('sha256', secret)
  return sha256Hasher.update(str).digest('hex').slice(0, n)
}

/**
 * Autogenerates a key for RDMO from a given text string
*/
export function text (text: string): string {
  return text
    .trim()
    .replace(/\s/g, '_')
    .toLowerCase()
    .replace(/[ä]/g, 'ae')
    .replace(/[ö]/g, 'oe')
    .replace(/[ü]/g, 'ue')
    .replace(/[ß]/g, 'ss')
    .replace(/[^a-zA-Z0-9_]/g, '')
}

export function condition (condition: Condition): string {
  const str = JSON.stringify(condition)
  return hash(str)
}
