import { Endpoint, Path, Reference } from './reference'

export type ReferenceMap = Map<string, Reference>

/**
 * Generates and tracks RDMO references used in JSON data
*/
export class ReferenceManager {
  group: string
  version: string
  definitions: ReferenceMap = new Map()
  references: ReferenceMap = new Map()
  /**
   * @param endpoint - The endpoint an instance tracks references for
   * @param prefix - The reference prefix
   * @param [config] - Stable parameters to customize and disambiguate the created references
   * @param [config.group] - If present, this is prepended to the first path segment
   * @param [config.version] - If present, this is appended to each path segment
  */
  constructor (public endpoint: Endpoint, public prefix: string, { group = '', version = '' }: { [K in 'group' | 'version']?: string } = {}) {
    this.group = group
    this.version = version
  }

  /**
  * Checks if a reference was already used
 */
  isDefined (path: Path): boolean {
    return this.definitions.has(path.join('/'))
  }

  /**
  * Checks if a reference was already used
 */
  isUsed (path: Path): boolean {
    return this.references.has(path.join('/'))
  }

  /**
   * Checks if there are used references that were never defined
  */
  isValid (): boolean {
    if (this.references.size > this.definitions.size) { return false }
    for (const [path] of this.references) {
      if (!this.definitions.has(path)) { return false }
    }
    return true
  }

  /**
   * Creates a new reference definition and returns the reference
  */
  define (path: Path): Reference {
    const key = path.join('/')
    if (this.definitions.has(key)) { throw new Error('Duplicate definition') }
    const reference = this.references.get(key) ?? this.generate(path)
    this.definitions.set(key, reference)
    return reference
  }

  /**
   * Registers a reference as used and returns the reference
 */
  use (path: Path): Reference {
    const key = path.join('/')
    if (this.references.has(key)) { return this.references.get(key) as Reference }
    const reference = this.definitions.get(key) ?? this.generate(path)
    this.references.set(key, reference)
    return reference
  }

  /**
   * Creates a new customized reference from given path segments
 */
  private generate (path: Path): Reference {
    return new Reference(
      this.prefix, this.endpoint,
      path.map((x, i) => {
        const segment = [x]
        if (i === 0) {
          if (this.group !== '') { segment.unshift(this.group) }
          if (this.version !== '') { segment.push(this.version) }
        }
        return segment.join('_')
      })
    )
  }
}
