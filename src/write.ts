import Ajv from 'ajv'
import type { Catalog, Condition, Domain, Question, ResponseConfiguration } from '@rdmo-author/schema'
import { validate } from '@rdmo-author/schema'
import type { Node, Range, AnswerType } from './serialize'

import * as serialize from './serialize'
import * as autokey from './key'
import { Endpoint, Reference } from './reference'
import { ReferenceState } from './state'
const ajv = new Ajv()

/**
 * Write RDMO catalog to XML
 * @param catalog - A catalog instance
*/
function write (catalog: Catalog): { [K in Endpoint | 'full']: string } {
  if (!validate(catalog)) { throw new Error(ajv.errorsText(validate.errors)) }
  const uris = new ReferenceState(catalog.prefix, { group: catalog.key, version: catalog.version })
  const nodes: { [K in Endpoint]: Set<Node> } = {
    domain: new Set(),
    options: new Set(),
    conditions: new Set(),
    questions: new Set()
  }
  if (catalog.domain != null) {
    const attributes = flattenDomain(catalog.domain)
    attributes.forEach(({ key, parentPath }) => {
      const reference = uris.domain.define(parentPath.concat(key))
      if (parentPath.length === 0) {
        nodes.domain.add(serialize.attribute({ reference }))
      } else {
        nodes.domain.add(serialize.attribute({ reference }, uris.domain.use(parentPath)))
      }
    })
  }
  if (catalog.optionsets != null) {
    Object.entries(catalog.optionsets).forEach(([key, options]) => {
      const reference = uris.options.define([key])
      nodes.options.add(serialize.optionset({ reference }))
      options.forEach((option, order) => {
        const optionKey = option.key ?? `${key}_${order}`
        const optionRef = uris.options.define([key, optionKey])
        const node = serialize.option({ reference: optionRef, order }, reference, option)
        nodes.options.add(node)
      })
    })
  }
  const catalogRef = uris.questions.define([catalog.key])
  nodes.questions.add(serialize.catalog({ reference: catalogRef }, catalog.title))
  if (catalog.sections != null) {
    catalog.sections.forEach((section, order) => {
      const sectionRef = uris.questions.define([catalog.key, section.key])
      const node = serialize.section({ reference: sectionRef, order }, catalogRef, section.title)
      nodes.questions.add(node)
      if (section.questionsets == null) { return }
      section.questionsets.forEach((questionset, order) => {
        const conditions: Reference[] = questionset.conditions?.map(condition => visitCondition(condition, uris, nodes.conditions)) ?? []
        const questionsetRef = uris.questions.define([catalog.key, section.key, questionset.key])
        if (typeof questionset.attribute === 'string') {
          const entity = uris.domain.use(questionset.attribute.split('/'))
          const node = serialize.questionset(
            { reference: questionsetRef, order }, sectionRef, questionset, conditions, { attribute: entity, name: '', pluralName: '' }
          )
          nodes.questions.add(node)
        } else {
          const node = serialize.questionset(
            { reference: questionsetRef, order }, sectionRef, questionset, conditions
          )
          nodes.questions.add(node)
        }
        questionset.questions?.forEach((question, order) => {
          const attribute = uris.domain.use(question.attribute.split('/'))
          const conditions: Reference[] = question.conditions?.map(condition => visitCondition(condition, uris, nodes.conditions)) ?? []
          const optionsets: Reference[] = []
          if (question.type === 'single-choice' || question.type === 'multiple-choice') {
            if (typeof question.options === 'string') {
              optionsets.push(uris.options.use([question.options]))
            } else {
              const optionsetRef = uris.options.define([question.key])
              nodes.options.add(serialize.optionset({ reference: optionsetRef, order: 0 }))
              optionsets.push(optionsetRef)
              question.options.forEach((option, order) => {
                const optionKey = option.key ?? `${question.key}_${order}`
                const optionRef = uris.options.define([question.key, optionKey])
                const node = serialize.option({ reference: optionRef, order }, optionsetRef, option)
                nodes.options.add(node)
              })
            }
          }
          const questionRef = uris.questions.define([catalog.key, section.key, questionset.key, question.key])
          const node = serialize.question(
            { reference: questionRef, order }, questionsetRef, attribute, question, { ...answerProperties(question), optionsets }, conditions
          )
          nodes.questions.add(node)
        })
      })
    })
  }
  uris.validate()
  return {
    domain: serialize.root(nodes.domain).end({ prettyPrint: true }),
    options: serialize.root(nodes.options).end({ prettyPrint: true }),
    conditions: serialize.root(nodes.conditions).end({ prettyPrint: true }),
    questions: serialize.root(nodes.questions).end({ prettyPrint: true }),
    full: serialize.root(new Set([...nodes.domain, ...nodes.options, ...nodes.conditions, ...nodes.questions])).end({ prettyPrint: true })
  }
}

type DomainItems = Array<{key: string, parentPath: string[]}>
function flattenDomain (domain: Domain, items: DomainItems = [], parentPath: string[] = []): DomainItems {
  domain.forEach(attribute => {
    if (typeof attribute === 'string') {
      items.push({ key: attribute, parentPath })
    } else {
      Object.entries(attribute).forEach(([entity, children]) => {
        items.push({ key: entity, parentPath })
        flattenDomain(children, items, parentPath.concat(entity))
      })
    }
  })
  return items
}

function answerProperties (question: Question): AnswerType {
  const config: AnswerType = {
    widget: 'text',
    value: 'text'
  }
  switch (question.type) {
    case 'yes-no':
      config.widget = 'yesno'
      config.value = 'boolean'
      break
    case 'single-response':
      if (question.response == null) { break }
      Object.assign(config, responseType(question.response))
      break
    case 'multiple-response':
      config.collection = { name: '', pluralName: '' }
      if (question.response == null) { break }
      Object.assign(config, responseType(question.response))
      break
    case 'single-choice': {
      config.value = 'option'
      switch (question.style) {
        case 'radio':
          config.widget = 'radio'
          break
        case 'dropdown':
          config.widget = 'select'
          break
        default:
          config.widget = 'radio'
      }
      break
    }
    case 'multiple-choice':
      config.value = 'option'
      config.collection = { name: '', pluralName: '' }
      switch (question.style) {
        case 'checkbox':
          config.widget = 'checkbox'
          break
        case 'dropdown':
          config.widget = 'select'
          break
        default:
          config.widget = 'checkbox'
      }
      break
  }
  return config
}

function responseType (response: ResponseConfiguration): AnswerType {
  const config: AnswerType = { widget: 'text', value: 'text' }
  switch (response.format) {
    case 'text':
      config.widget = response.multiline === true ? 'textarea' : 'text'
      break
    case 'integer':
      config.value = 'integer'
      if (response.unit != null) { config.unit = response.unit }
      if (response.range != null) {
        config.widget = 'range'
        config.range = range(response.range)
      }
      break
    case 'decimal':
      config.value = 'float'
      if (response.unit != null) { config.unit = response.unit }
      if (response.range != null) {
        config.widget = 'range'
        config.range = range(response.range)
      }
      break
    case 'date':
      config.value = 'datetime'
      config.widget = 'date'
      break
  }
  return config
}

function range ([minimum, maximum, step]: [number, number, number]): Range {
  return { minimum, maximum, step }
}

function visitCondition (condition: Condition, uris: ReferenceState, nodes: Set<Node>): Reference {
  const key = autokey.condition(condition)
  const reference: Reference = uris.conditions.use([key])
  if (uris.conditions.isDefined([key])) { return reference }
  const source = uris.domain.use(condition.slice(0, 1))
  let node
  if (condition[2] === 'option') {
    node = serialize.condition({ reference }, source, condition[1], uris.options.use(condition[3].split('/')))
  } else {
    node = serialize.condition({ reference }, source, condition[1], condition[3])
  }
  uris.conditions.define([key])
  nodes.add(node)
  return reference
}

export { write, flattenDomain, answerProperties }
