import test from 'ava'
import dedent from 'dedent-js'
import { fragment } from 'xmlbuilder2'
import { Endpoint, Reference } from './reference'
import * as serialize from './serialize'
const prefix = 'https://rdmo.example.com/terms'

test('root', t => {
  t.is(
    serialize.root().end({ prettyPrint: true }),
    dedent`
    <?xml version="1.0" encoding="UTF-8"?>
    <rdmo xmlns:dc="http://purl.org/dc/elements/1.1/"/>
    `
  )
})

test('root with contents', t => {
  const contents = new Map()
  contents.set(new Reference('rdmo.example.com', Endpoint.domain, 'attribute'), fragment().ele('attribute'))
  t.is(
    serialize.root(contents).end({ prettyPrint: true }),
    dedent`
    <?xml version="1.0" encoding="UTF-8"?>
    <rdmo xmlns:dc="http://purl.org/dc/elements/1.1/">
      <attribute/>
    </rdmo>
    `
  )
})

test('attribute without parent', t => {
  const reference = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  t.is(
    serialize.attribute({ reference }).end({ prettyPrint: true }),
    dedent`
    <attribute dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>attribute</key>
      <path>entity/attribute</path>
      <dc:comment/>
      <parent/>
    </attribute>
    `
  )
})

test('Attribute with parent', t => {
  const reference = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  const parentRef = new Reference(prefix, Endpoint.domain, 'entity')
  t.is(
    serialize.attribute({ reference, comment: 'comment' }, parentRef).end({ prettyPrint: true }),
    dedent`
    <attribute dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>attribute</key>
      <path>entity/attribute</path>
      <dc:comment>comment</dc:comment>
      <parent dc:uri="${parentRef.uri}"/>
    </attribute>
    `
  )
})

test('Condition with text target', t => {
  const reference = new Reference(prefix, Endpoint.conditions, 'foo_bar')
  const source = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  t.is(
    serialize.condition({ reference }, source, 'equals', 'bar').end({ prettyPrint: true }),
    dedent`
    <condition dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>foo_bar</key>
      <dc:comment/>
      <source dc:uri="${source.uri}"/>
      <relation>equals</relation>
      <target_text>bar</target_text>
      <target_option/>
    </condition>
    `
  )
})

test('Condition with option target', t => {
  const reference = new Reference(prefix, Endpoint.conditions, 'foo_bar')
  const source = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  const target = new Reference(prefix, Endpoint.options, 'optionset/option')
  t.is(
    serialize.condition({ reference }, source, 'equals', target).end({ prettyPrint: true }),
    dedent`
    <condition dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>foo_bar</key>
      <dc:comment/>
      <source dc:uri="${source.uri}"/>
      <relation>equals</relation>
      <target_text/>
      <target_option dc:uri="${target.uri}"/>
    </condition>
    `
  )
})

test('Optionset', t => {
  const reference = new Reference(prefix, Endpoint.options, 'foo_bar')
  t.is(
    serialize.optionset({ reference })
      .end({ prettyPrint: true }),
    dedent`
    <optionset dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>foo_bar</key>
      <dc:comment/>
      <order>0</order>
      <provider_key/>
      <conditions/>
    </optionset>
    `
  )
})

test('Optionset with order', t => {
  const reference = new Reference(prefix, Endpoint.options, 'foo_bar')
  t.is(
    serialize.optionset({ reference, order: 42 })
      .end({ prettyPrint: true }),
    dedent`
    <optionset dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>foo_bar</key>
      <dc:comment/>
      <order>42</order>
      <provider_key/>
      <conditions/>
    </optionset>
    `
  )
})

test('Optionset with conditions', t => {
  const reference = new Reference(prefix, Endpoint.options, 'foo_bar')
  const conditions = [
    new Reference(prefix, Endpoint.conditions, 'cond_1'),
    new Reference(prefix, Endpoint.conditions, 'cond_2')
  ]
  t.is(
    serialize.optionset({ reference, order: 0 }, conditions).end({ prettyPrint: true }),
    dedent`
    <optionset dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>foo_bar</key>
      <dc:comment/>
      <order>0</order>
      <provider_key/>
      <conditions>
        <condition dc:uri="${conditions[0].uri}"/>
        <condition dc:uri="${conditions[1].uri}"/>
      </conditions>
    </optionset>
    `
  )
})

test('option', t => {
  const reference = new Reference(prefix, Endpoint.options, 'foo/bar')
  const optionset = new Reference(prefix, Endpoint.options, 'foo')
  t.is(
    serialize.option({ reference, order: 5 }, optionset, { text: 'Option text' }).end({ prettyPrint: true }),
    dedent`
    <option dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>bar</key>
      <path>foo/bar</path>
      <dc:comment/>
      <order>5</order>
      <optionset dc:uri="${optionset.uri}"/>
      <text lang="de">Option text</text>
      <text lang="en">Option text</text>
      <additional_input>False</additional_input>
    </option>
    `
  )
})

test('option with additional input', t => {
  const reference = new Reference(prefix, Endpoint.options, 'foo/bar')
  const optionset = new Reference(prefix, Endpoint.options, 'foo')
  t.is(
    serialize.option({ reference, order: 5 }, optionset, { text: 'Option text', additionalInput: true })
      .end({ prettyPrint: true }),
    dedent`
    <option dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>bar</key>
      <path>foo/bar</path>
      <dc:comment/>
      <order>5</order>
      <optionset dc:uri="${optionset.uri}"/>
      <text lang="de">Option text</text>
      <text lang="en">Option text</text>
      <additional_input>True</additional_input>
    </option>
    `
  )
})

test('option with additional input and translated text', t => {
  const reference = new Reference(prefix, Endpoint.options, 'foo/bar')
  const optionset = new Reference(prefix, Endpoint.options, 'foo')
  const text = { en: 'Option text', de: 'Text der Option' }
  t.is(
    serialize.option({ reference, order: 5 }, optionset, { text, additionalInput: true }).end({ prettyPrint: true }),
    dedent`
    <option dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>bar</key>
      <path>foo/bar</path>
      <dc:comment/>
      <order>5</order>
      <optionset dc:uri="${optionset.uri}"/>
      <text lang="de">Text der Option</text>
      <text lang="en">Option text</text>
      <additional_input>True</additional_input>
    </option>
    `
  )
})

test('catalog', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo')
  t.is(
    serialize.catalog({ reference }, { en: 'Catalog title', de: 'Titel des Katalogs' })
      .end({ prettyPrint: true }),
    dedent`
    <catalog dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>foo</key>
      <dc:comment/>
      <order>0</order>
      <title lang="de">Titel des Katalogs</title>
      <title lang="en">Catalog title</title>
    </catalog>
    `
  )
})

test('catalog with order', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo')
  t.is(
    serialize.catalog({ reference, order: 42 }, { en: 'Catalog title', de: 'Titel des Katalogs' })
      .end({ prettyPrint: true }),
    dedent`
    <catalog dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>foo</key>
      <dc:comment/>
      <order>42</order>
      <title lang="de">Titel des Katalogs</title>
      <title lang="en">Catalog title</title>
    </catalog>
    `
  )
})

test('section', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo/bar')
  const catalog = new Reference(prefix, Endpoint.questions, 'foo')
  t.is(
    serialize.section({ reference, order: 5 }, catalog, { en: 'Section title', de: 'Titel des Abschnitts' }).end({ prettyPrint: true }),
    dedent`
    <section dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>bar</key>
      <path>foo/bar</path>
      <dc:comment/>
      <order>5</order>
      <catalog dc:uri="${catalog.uri}"/>
      <title lang="de">Titel des Abschnitts</title>
      <title lang="en">Section title</title>
    </section>
    `
  )
})

test('questionset', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo/bar/baz')
  const section = new Reference(prefix, Endpoint.questions, 'foo/bar')
  t.is(
    serialize.questionset({ reference, order: 5 }, section, { title: 'Questionset title' }).end({ prettyPrint: true }),
    dedent`
    <questionset dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>baz</key>
      <path>foo/bar/baz</path>
      <dc:comment/>
      <order>5</order>
      <attribute/>
      <section dc:uri="${section.uri}"/>
      <is_collection>False</is_collection>
      <help lang="de"/>
      <help lang="en"/>
      <title lang="de">Questionset title</title>
      <title lang="en">Questionset title</title>
      <verbose_name lang="de"/>
      <verbose_name lang="en"/>
      <verbose_name_plural lang="de"/>
      <verbose_name_plural lang="en"/>
      <conditions/>
    </questionset>
    `
  )
})

test('Questionset as collection', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo/bar/baz')
  const section = new Reference(prefix, Endpoint.questions, 'foo/bar')
  const attribute = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  t.is(
    serialize.questionset({ reference, order: 5 }, section, { title: 'Questionset title', help: 'Help text' }, [], { attribute }).end({ prettyPrint: true }),
    dedent`
    <questionset dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>baz</key>
      <path>foo/bar/baz</path>
      <dc:comment/>
      <order>5</order>
      <attribute dc:uri="${attribute.uri}"/>
      <section dc:uri="${section.uri}"/>
      <is_collection>True</is_collection>
      <help lang="de">Help text</help>
      <help lang="en">Help text</help>
      <title lang="de">Questionset title</title>
      <title lang="en">Questionset title</title>
      <verbose_name lang="de"/>
      <verbose_name lang="en"/>
      <verbose_name_plural lang="de"/>
      <verbose_name_plural lang="en"/>
      <conditions/>
    </questionset>
    `
  )
})

test('Questionset with conditions', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo/bar/baz')
  const section = new Reference(prefix, Endpoint.questions, 'foo/bar')
  const condition = new Reference(prefix, Endpoint.conditions, 'foo_bar')
  t.is(
    serialize.questionset({ reference, order: 5 }, section, { title: 'Questionset title', help: 'Help text' }, [condition]).end({ prettyPrint: true }),
    dedent`
    <questionset dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>baz</key>
      <path>foo/bar/baz</path>
      <dc:comment/>
      <order>5</order>
      <attribute/>
      <section dc:uri="${section.uri}"/>
      <is_collection>False</is_collection>
      <help lang="de">Help text</help>
      <help lang="en">Help text</help>
      <title lang="de">Questionset title</title>
      <title lang="en">Questionset title</title>
      <verbose_name lang="de"/>
      <verbose_name lang="en"/>
      <verbose_name_plural lang="de"/>
      <verbose_name_plural lang="en"/>
      <conditions>
        <condition dc:uri="${condition.uri}"/>
      </conditions>
    </questionset>
    `
  )
})

test('Question with text input', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo/bar/baz/42')
  const questionset = new Reference(prefix, Endpoint.questions, 'foo/bar/baz')
  const attribute = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  t.is(
    serialize.question({ reference, order: 5 }, questionset, attribute, { text: 'Question text', help: 'Help text' }, { value: 'text', widget: 'text' }).end({ prettyPrint: true }),
    dedent`
    <question dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>42</key>
      <path>foo/bar/baz/42</path>
      <dc:comment/>
      <order>5</order>
      <attribute dc:uri="${attribute.uri}"/>
      <questionset dc:uri="${questionset.uri}"/>
      <is_collection>False</is_collection>
      <help lang="de">Help text</help>
      <help lang="en">Help text</help>
      <text lang="de">Question text</text>
      <text lang="en">Question text</text>
      <verbose_name lang="de"/>
      <verbose_name lang="en"/>
      <verbose_name_plural lang="de"/>
      <verbose_name_plural lang="en"/>
      <widget_type>text</widget_type>
      <value_type>text</value_type>
      <minimum/>
      <maximum/>
      <step/>
      <unit/>
      <optionsets/>
      <conditions/>
    </question>
    `
  )
})

test('Question with number input', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo/bar/baz/42')
  const questionset = new Reference(prefix, Endpoint.questions, 'foo/bar/baz')
  const attribute = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  t.is(
    serialize.question({ reference, order: 5 }, questionset, attribute, { text: 'Question text', help: 'Help text' }, { value: 'number', widget: 'range', unit: 'cm', range: { minimum: 0, maximum: 10 } }).end({ prettyPrint: true }),
    dedent`
    <question dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>42</key>
      <path>foo/bar/baz/42</path>
      <dc:comment/>
      <order>5</order>
      <attribute dc:uri="${attribute.uri}"/>
      <questionset dc:uri="${questionset.uri}"/>
      <is_collection>False</is_collection>
      <help lang="de">Help text</help>
      <help lang="en">Help text</help>
      <text lang="de">Question text</text>
      <text lang="en">Question text</text>
      <verbose_name lang="de"/>
      <verbose_name lang="en"/>
      <verbose_name_plural lang="de"/>
      <verbose_name_plural lang="en"/>
      <widget_type>range</widget_type>
      <value_type>number</value_type>
      <minimum>0</minimum>
      <maximum>10</maximum>
      <step/>
      <unit>cm</unit>
      <optionsets/>
      <conditions/>
    </question>
    `
  )
})

test('Question (multiple-choice)', t => {
  const reference = new Reference(prefix, Endpoint.questions, 'foo/bar/baz/42')
  const questionset = new Reference(prefix, Endpoint.questions, 'foo/bar/baz')
  const attribute = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  const optionset = new Reference(prefix, Endpoint.options, 'foo_bar')
  const condition = new Reference(prefix, Endpoint.conditions, 'foo_bar')
  t.is(
    serialize.question({ reference, comment: 'comment', order: 5 }, questionset, attribute, { text: 'Question text' }, { value: 'option', widget: 'checkbox', optionsets: [optionset], collection: { name: 'thing', pluralName: 'things' } }, [condition]).end({ prettyPrint: true }),
    dedent`
    <question dc:uri="${reference.uri}">
      <uri_prefix>${prefix}</uri_prefix>
      <key>42</key>
      <path>foo/bar/baz/42</path>
      <dc:comment>comment</dc:comment>
      <order>5</order>
      <attribute dc:uri="${attribute.uri}"/>
      <questionset dc:uri="${questionset.uri}"/>
      <is_collection>True</is_collection>
      <help lang="de"/>
      <help lang="en"/>
      <text lang="de">Question text</text>
      <text lang="en">Question text</text>
      <verbose_name lang="de">thing</verbose_name>
      <verbose_name lang="en">thing</verbose_name>
      <verbose_name_plural lang="de">things</verbose_name_plural>
      <verbose_name_plural lang="en">things</verbose_name_plural>
      <widget_type>checkbox</widget_type>
      <value_type>option</value_type>
      <minimum/>
      <maximum/>
      <step/>
      <unit/>
      <optionsets>
        <optionset dc:uri="${optionset.uri}"/>
      </optionsets>
      <conditions>
        <condition dc:uri="${condition.uri}"/>
      </conditions>
    </question>
    `
  )
})
