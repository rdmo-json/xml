import test from 'ava'
import * as key from './key'

test('letters and numbers with space', t => {
  t.is(key.text('foo bar'), 'foo_bar')
  t.is(key.text('foo 123'), 'foo_123')
})

test('letters and numbers', t => {
  t.is(key.text('foo123'), 'foo123')
})

test('Keep underscores', t => {
  t.is(key.text('foo_123'), 'foo_123')
})

test('Uppercase letters', t => {
  t.is(key.text('Foo Bar'), 'foo_bar')
})

test('remove disallowed characters', t => {
  t.is(key.text('Foo, bar, and baz.'), 'foo_bar_and_baz')
})

test('remove umlauts', t => {
  t.is(key.text('Föö, bär, and büz.'), 'foeoe_baer_and_buez')
})

test('condition with option target', t => {
  t.is(
    key.condition(['entity/attribute', 'eq', 'option', 'optionset/option']),
    '333fa2dd4f'
  )
})
