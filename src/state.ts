import { Endpoint } from './reference'
import { ReferenceManager } from './reference-manager'

export class ReferenceState {
  conditions: ReferenceManager
  domain: ReferenceManager
  options: ReferenceManager
  questions: ReferenceManager
  constructor (prefix: string, { group = '', version = '' }: { [K in 'group' | 'version' ]?: string } = {}) {
    this.conditions = new ReferenceManager(Endpoint.conditions, prefix, { group, version })
    this.domain = new ReferenceManager(Endpoint.domain, prefix, { group, version })
    this.options = new ReferenceManager(Endpoint.options, prefix, { group, version })
    this.questions = new ReferenceManager(Endpoint.questions, prefix, { version })
  }

  validate (): boolean {
    if (!this.domain.isValid()) { throw new Error('Invalid domain uris') }
    if (!this.conditions.isValid()) { throw new Error('Invalid conditions uris') }
    if (!this.options.isValid()) { throw new Error('Invalid options uris') }
    if (!this.questions.isValid()) { throw new Error('Invalid questions uris') }
    return true
  }
}
