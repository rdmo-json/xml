import test from 'ava'
import { Endpoint, Reference } from './reference'
const prefix = 'https://rdmo.example.com/terms'

test('key', t => {
  const ref = new Reference(prefix, Endpoint.domain, ['entity', 'attribute'])
  t.is(ref.key, 'attribute')
})

test('path', t => {
  const ref = new Reference(prefix, Endpoint.domain, ['entity', 'attribute'])
  t.is(ref.path, 'entity/attribute')
})

test('parent path', t => {
  const ref = new Reference(prefix, Endpoint.questions, ['catalog', 'section', 'questionset', 'question'])
  t.is(ref.parentPath, 'catalog/section/questionset')
})

test('path parsing', t => {
  const ref = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  t.is(ref.key, 'attribute')
})

test('uri', t => {
  const ref = new Reference(prefix, Endpoint.domain, 'entity/attribute')
  t.is(ref.uri, 'https://rdmo.example.com/terms/domain/entity/attribute')
})

test('empty path should throw', t => {
  t.throws(() => new Reference(prefix, Endpoint.domain, []))
})
