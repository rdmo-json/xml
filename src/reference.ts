export type Path = string[]
export enum Endpoint {
  conditions = 'conditions',
  domain = 'domain',
  options = 'options',
  questions = 'questions'
}

/**
 * Represents a reference to an rDMO element
*/
export class Reference {
  pathSegments: string[] = []
  constructor (public prefix: string, public endpoint: Endpoint, path: Path|string) {
    if (path.length === 0) { throw new Error('Path must not be empty') }
    this.prefix = prefix
    this.endpoint = endpoint
    if (typeof path === 'string') {
      this.pathSegments = path.split('/')
    } else {
      this.pathSegments = path
    }
  }

  get key (): string {
    return this.pathSegments[this.pathSegments.length - 1]
  }

  get path (): string {
    return this.pathSegments.join('/')
  }

  get parentPath (): string {
    return this.pathSegments.slice(0, -1).join('/')
  }

  toString (): string {
    return [this.prefix, this.endpoint, this.path].join('/')
  }

  get uri (): string {
    return this.toString()
  }
}
