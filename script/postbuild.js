'use strict'
const { writeFileSync } = require('fs')
const packages = {
  './dist/commonjs/package.json': { type: 'commonjs' },
  './dist/esm/package.json': { type: 'module' }
}

for (const [name, content] of Object.entries(packages)) {
  writeFileSync(name, JSON.stringify(content, null, 2))
}
