'use strict'
const { writeFileSync } = require('fs')
const { write } = require('../')
const catalog = require('../integration/catalog.json')
Object.entries(write(catalog))
  .forEach(
    ([k, v]) => writeFileSync(`integration/fixtures/${k}.xml`, v)
  )
